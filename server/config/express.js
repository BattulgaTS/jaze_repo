var express             = require('express'),
    morgan              = require('morgan'),
    path                = require('path'),
    config              = require('./index'),
    bodyParser          = require('body-parser')

module.exports = function(app) {

  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json());

    
  app.use(express.static(path.join(config.root, '/client/dist')))
  app.use(express.static(path.join(config.root, '/client/src')))
  app.use(express.static(path.join(config.root, '/client/.tmp/serve')))
  app.use(morgan('dev'));
}
