var path = require('path')
var all = {
  // Root path of server
  root: path.normalize(__dirname + '/../..'),

  // Server port
  port: 8080,

  // Server IP
  ip: '0.0.0.0'

};

module.exports = all;
