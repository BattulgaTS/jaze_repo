var path      = require('path')
var config    = require('./config')
var express   = require('express')
var multer    = require('multer')
var request   = require('superagent')

var routes = function(app) {

  app.use(function(req, res, next){
    res.setHeader('Access-Control-Allow-Origin', 'http://www.tulgadev.com');
    res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
  })
  var storage = multer.diskStorage({
    destination: function(req, file, cb) {
      cb(null, __dirname + '/images/')
    },
    filename: function(req, file, cb) {
      cb(null, file.originalname)
    }
  })
  var upload = multer({ storage: storage })

  app.use('/api/plate', upload.single('plate'), require('./api/plate'))
  app.use('/api/vin', require('./api/vin'))

  // app.use('/api/vin', upload.single('vin'), require('./api/vin'))



  app.use('/api/images', express.static(__dirname + '/images'))

  app.route('/*')
    .get(function(req, res) {
      return res.sendFile(path.resolve(config.root, 'client/.tmp/serve/index.html'));
    });


};

module.exports = routes;
