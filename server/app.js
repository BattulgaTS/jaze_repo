var express             = require('express'),
    http                = require('http'),
    config              = require('./config')



var app = express();

require('./config/express')(app)
require('./routes')(app)
var server = http.createServer(app)

server.listen(config.port, function() {
  console.log('Express server listening on %d, in %s mode', config.port, app.get('env'));
});



/*
  TEXTRACT test
*/
// var textract = require('textract')
//
// textract.fromFileWithPath('./VIN2b.jpg', function (err, text){
//   if (err){
//     console.log(err)
//   } else {
//     console.log(text)
//   }
// })
