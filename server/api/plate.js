var Router = require('express').Router
var router = new Router()
var request = require('superagent')

router.post ('/' , function(req, res, next){

  console.log('req.file', req.file)
  console.log('req.body', req.body)

  request
      .post('https://api.openalpr.com/v1/recognize')
      .query({secret_key: 'sk_d2a8088a3502d65effaafe7e'})
      .query({tasks: 'plate'})
      .query({country: 'us'})
      .set('Content-Type', 'multipart/form-data')
      .set('Accept', 'application/json')
      .attach('image', req.file.path)
      .end(function (error, response) {
        if(!error && response.statusCode == 200) {
          // console.log(JSON.stringify(response.body, null, ' '))
          response.body.filename = req.file.originalname;
          res.json(response.body)
        } else {
          console.log(error);
          res.end();
        }
      })

})

module.exports = router;
