(function() {
  'use strict';

  angular
    .module('jaze')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider
      .state('main', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      })

    $urlRouterProvider.otherwise('/');
    $locationProvider.html5Mode(true);
  }

})();
