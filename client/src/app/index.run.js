(function() {
  'use strict';

  angular
    .module('jaze')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {
    $log.debug('runBlock end');
  }

})();
