(function() {
  'use strict';

  angular
    .module('jaze')
    .directive('digitalForm', digitalForm)
    .directive('fileModel', fileModel);

  /** @ngInject */
  function digitalForm() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/jazeform/jazeform.html',
      controller: ['send', '$log', 'Notification',FormController],
      controllerAs: 'fCont'
    };

    return directive;

    // FormController.$inject = ['send', '$log', 'Notification'];
    function FormController (send, $log, Notification) {
      var vm = this;

      vm.vehicle = {};
      vm.plate = {
        test: 'test'
      };
      vm.plateShow = true;
      vm.platePic = 'http://localhost:8081/api/images/1.jpg';

      vm.sendPlate = function(){
        send.request('/plate', 'POST', vm.plate, true)
        .then(
          function(res){
            vm.API_res = res
            if (res.plate.results[0]){
              Notification.success('Scanned Successfully');
              vm.vehicle.plateNumber = res.plate.results[0].plate;
              vm.vehicle.plateState = res.plate.results[0].region.toUpperCase();
            } else {
              Notification.error('Scan Failed, Please upload other picture or type in manually');
            }
          },
          function(err){
            $log.debug(err)
        })
      }

      vm.sendVIN = function(){
        send.request('/vin', 'POST', vm.vin)
        .then(
          function(res){
            vm.API_res = res
            if (res.success) {
              Notification.success('Filled Successfully');
              vm.vehicle.year = res.specification.year;
              vm.vehicle.make = res.specification.make;
              vm.vehicle.model = res.specification.model;
              vm.vehicle.city_mileage = res.specification.city_mileage;
              vm.vehicle.highway_mileage = res.specification.highway_mileage;
              vm.vehicle.vin = res.specification.vin;
            } else {
              Notification.error('Something went wrong');
            }

          },
          function(err){
            Notification.error('Something went wrong');
            $log.debug(err)
        })
      }
    }
  }
  function fileModel ($parse){
    return {
      restrict: 'A',
      link: function(scope, element, attrs){
        var model = $parse(attrs.fileModel);
        var modelSetter =  model.assign;

        element.bind('change', function(){
          scope.$apply(function(){
            modelSetter(scope, element[0].files[0]);
          })
        })
      }
    }
  }

})();
