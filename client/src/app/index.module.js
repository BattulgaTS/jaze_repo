(function() {
  'use strict';

  angular
    .module('jaze', ['ngAnimate',
                       'ngCookies',
                       'ngTouch',
                       'ngSanitize',
                       'ngMessages',
                       'ngAria',
                       'ngResource',
                       'ui.router',
                       'ui.bootstrap',
                       'angularMoment',
                       'ui-notification'
                      ]);

})();
