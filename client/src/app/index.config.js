(function() {
  'use strict';

  angular
    .module('jaze')
    .config(configuration);

  /** @ngInject */
  function configuration($logProvider, NotificationProvider) {
    /* Notification Configurations */
    NotificationProvider.setOptions({
      delay: 3000,
      startTop: 20,
      startRight: 10,
      verticalSpacing: 20,
      horizontalSpacing: 20,
      positionX: 'center',
      positionY: 'top'
    });

    // Enable log
    $logProvider.debugEnabled(true);

  }

})();
