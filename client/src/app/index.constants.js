(function() {
  'use strict';

  var config = {
    app: {
      host: 'http://50.112.43.237'
    },
    dev: {
      host: 'http://localhost:8080'
    },
    env: 'app'
  }

  var appConfig = {
    userRoles: ['guest', 'user', 'admin']
  }

  angular
    .module('jaze')
    .constant('config', config)
    .constant("appConfig", appConfig)

})();
